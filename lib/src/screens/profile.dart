import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListView(
        children: <Widget>[
          Stack(
            clipBehavior: Clip.none, alignment: Alignment.center,
            children: const <Widget>[
              Image(
                image: AssetImage("assets/images/bg.jpg"),
                fit: BoxFit.cover,
              ),
              Positioned(
                bottom: -100,
                child: CircleAvatar(
                  radius: 100,
                  backgroundColor: Colors.white,
                  backgroundImage: AssetImage("assets/images/profile.png"),
                )
              )
            ],
          ),
          const SizedBox(
            height: 140
          ),
          const ListTile(
            title: Center(
              child: Text("Nombre"),
            ),
            subtitle: Center(
              child: Text("Elmer Israel"),
            ),
          ),
          const ListTile(
            title: Center(
              child: Text("Apellido"),
            ),
            subtitle: Center(
              child: Text("Machaca Chura"),
            ),
          ),
          const ListTile(
            title: Center(
              child: Text("Correo electrónico"),
            ),
            subtitle: Center(
              child: Text("is.elmer.inf@gmail.com"),
            ),
          )
        ],
      ),
    );
  }
}
